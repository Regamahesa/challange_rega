package com.example.challange.serviceImpl;

import com.example.challange.model.Register;
import com.example.challange.repository.RegisterRepository;
import com.example.challange.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private RegisterRepository registerRepository;

    @Override
    public Map<String, Object> Login(Register register) {
        Map<String, Object> response = new HashMap<>();
        response.put("email", register);
        response.put("password", register);
        return response;
    }

    @Override
    public Register addUser(Register register) {
    String email = register.getEmail();
    var validasi = registerRepository.findByEmail(email);
        return registerRepository.save(register);
    }

    @Override
    public List<Register> getAllRegister() {
        return registerRepository.findAll();
    }

    @Override
    public Object editRegister(Integer id, String username, String email, String password) {
        Register register = registerRepository.findById(id).get();
        register.setUsername(username);
        register.setEmail(email);
        register.setPassword(password);
        return registerRepository.save(register);
    }

    @Override
    public void deleteUserById(Integer id) {
    registerRepository.deleteById(id);
    }

}
