package com.example.challange.service;

import com.example.challange.model.Register;

import java.util.List;
import java.util.Map;

public interface RegisterService {

    Map<String, Object> Login(Register register);

    Register addUser(Register register);

    List<Register> getAllRegister();

    Object editRegister(Integer id, String username, String email, String password);

    void deleteUserById(Integer id);
}
