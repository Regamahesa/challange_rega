package com.example.challange.controller;

import com.example.challange.model.Register;
import com.example.challange.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3000")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping
    public List<Register> getAllUser() {
        return registerService.getAllRegister();
    }

    @PostMapping("/sign-in")
    public Map<String, Object> login(@RequestBody Register register) {
        return registerService.Login(register);
    }

    @PostMapping
    public Register addUser(@RequestBody Register register) {
      return registerService.addUser(register);

    }

    @PutMapping("/{Id}")
    public Object editUserById(@PathVariable("Id") Integer id, @RequestBody Register register) {
        return registerService.editRegister(id, register.getUsername(), register.getEmail(), register.getPassword());
    }

    @DeleteMapping("/{Id}")
    public void deleteById(@PathVariable("Id") Integer id) {
        registerService.deleteUserById(id);
    }

}
