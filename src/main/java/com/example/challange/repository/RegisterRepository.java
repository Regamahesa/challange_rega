package com.example.challange.repository;

import com.example.challange.model.Register;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RegisterRepository extends JpaRepository<Register, Integer> {
//    Untuk findByEmail
    Optional<Register> findByEmail(String email);
}
